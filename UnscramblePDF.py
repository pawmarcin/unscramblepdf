from PyPDF2 import PdfWriter
from PyPDF2 import PdfReader
from pathlib import Path
import os

print(os.getcwd())
# pobiera sciezke 

os.chdir('/Users/Pawel/Desktop/KursJunior/practice_files/ch14-interact-with-pdf-files/practice_files')
# zmienia sciezke 

pdf_home = (
    # nadanie sciezki 
    Path.home() / 
    "Desktop" /
    "KursJunior" /
    "practice_files" /
    "ch14-interact-with-pdf-files" /
    "practice_files" /
    "scrambled.pdf" 
)
def get_page_text(page):
    return page.extract_text()

pdf_r = PdfReader(str(pdf_home))
pdf_w = PdfWriter()

pages = list(pdf_r.pages)
pages.sort(key=get_page_text)

for page in pages:
    rotation_degrees = page["/Rotate"]
    print(page["/Rotate"])
    if rotation_degrees != 0:
        page.rotate(rotation_degrees * -1)
    pdf_w.add_page(page)

with Path("unscrambled.pdf").open(mode="wb") as output_file:
# zapis nowego pliku 
    pdf_w.write(output_file)